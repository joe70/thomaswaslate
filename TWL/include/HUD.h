/*
 * HUD.h
 *
 *  Created on: 4 abr. 2019
 *      Author: joe
 */

#ifndef INCLUDE_HUD_H_
#define INCLUDE_HUD_H_

#include <SFML/Graphics.hpp>

class Hud {
private:
	sf::Font m_Font;
	sf::Text m_StartText;
	sf::Text m_TimeText;
	sf::Text m_LevelText;
public:
	Hud();
	sf::Text getMessage();
	sf::Text getLevel();
	sf::Text getTime();

	void setLevel(sf::String text);
	void setTime(sf::String text);
};
#endif /* INCLUDE_HUD_H_ */
