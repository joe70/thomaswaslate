/*
 * Thomas.h
 *
 *  Created on: 21 mar. 2019
 *      Author: joe
 */

#ifndef INCLUDE_THOMAS_H_
#define INCLUDE_THOMAS_H_

#include "PlayableCharacter.h"

class Thomas : public PlayableCharacter {
public:
	// A constructor specific to Thomas
	Thomas();
	// Destructor virtual
	virtual ~Thomas();

	// The overridden input handler for Thomas
	bool virtual handleInput();
};

#endif /* INCLUDE_THOMAS_H_ */
