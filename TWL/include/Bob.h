/*
 * Bob.h
 *
 *  Created on: 21 mar. 2019
 *      Author: joe
 */

#ifndef INCLUDE_BOB_H_
#define INCLUDE_BOB_H_
#include "PlayableCharacter.h"

class Bob : public PlayableCharacter {
public:
	// A constructor specific to Bob
	Bob();

	// Destructor virtual
	virtual ~Bob();

	// The overriden input handler for Bob
	bool virtual handleInput();
};
#endif /* INCLUDE_BOB_H_ */
