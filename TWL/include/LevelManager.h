/*
 * LevelManager.h
 *
 *  Created on: 25 mar. 2019
 *      Author: joe
 */

#ifndef INCLUDE_LEVELMANAGER_H_
#define INCLUDE_LEVELMANAGER_H_
#include <SFML/Graphics.hpp>

class LevelManager {
private:
	sf::Vector2i m_LevelSize; // número tiles del nivel
	sf::Vector2f m_StartPosition; // coordenadas del mundo donde aparecen los personajes
	float m_TimeModifier = 1.0F;
	float m_BaseTimeLimit = 0.0F;
	int m_CurrentLevel = 0;
	const int NUM_LEVELS = 4U;

public:
	const int TILE_SIZE = 50U;
	const int VERTS_IN_QUAD = 4U;
	float getTimeLimit();
	sf::Vector2f getStartPosition();
	int ** nextLevel(sf::VertexArray& rVaLevel);
	sf::Vector2i getLevelSize();
	int getCurrentLevel();
};
#endif /* INCLUDE_LEVELMANAGER_H_ */
