/*
 * ParticleSystem.h
 *
 *  Created on: 18 abr. 2019
 *      Author: joe
 */

#ifndef INCLUDE_PARTICLESYSTEM_H_
#define INCLUDE_PARTICLESYSTEM_H_

#include "Particle.h"
#include <vector>

class ParticleSystem : public sf::Drawable {
private:
	std::vector<Particle> m_Particles;
	sf::VertexArray m_Vertices;
	float m_Duration;
	bool m_IsRunning;

public:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void init(int count);
	void emitParticles(sf::Vector2f position);
	void update(float elapsed);
	bool running();
};
#endif /* INCLUDE_PARTICLESYSTEM_H_ */
